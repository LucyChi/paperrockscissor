#ifndef _COMM_H__
#define _COMM_H__

#define CMD_CREATE      "create"
#define CMD_REG         "reg"
#define CMD_LIST        "list"
#define CMD_JOIN        "join"
#define CMD_EXIT        "exit"
#define CMD_DREG        "dereg"
#define CMD_MSG         "msg"

#endif
