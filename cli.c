#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <fcntl.h>
#include <pthread.h>

#include "comm.h"


#define cli_hint(fmt, args...) fprintf(stderr, fmt "#cli>" , \
             ##args)

#define cli_without_hint(fmt, args...) fprintf(stderr, fmt "" , \
             ##args)


//#define cli_hint printf
static int state = 0;
static int cli_fd = 0;

static void *cli_handle(void *arg)
{
	//int *fd = (int *)arg;
	char recvbuff[1024];
	int n;
	while (1) {
		n  = read(cli_fd, recvbuff, sizeof(recvbuff) - 1);	
		if (n < 0) {
			usleep(100);
			continue;	
		}
		if (n == 0) {
			close(cli_fd);
			return NULL;
		}
		recvbuff[n] = 0; 
	       	cli_hint("%s\n", recvbuff); }
}

static void cmd_help(void)
{
	printf("You can use follow command:\n"
			"\thelp\n"
			"\tlist\n"
			"\tcreate\n"
			"\treg\n"
			"\tjoin\n"
			"\tdereg\n"
			"\texit\n"
			);
}

static int err_check(void)
{
	if (cli_fd <= 0) {
		cli_hint("You are not connected to server\n");
		return 1;
	}
	return 0;
}

static void cmd_create(void)
{
	char sndbuff[2018];

	if (err_check()) return;
	snprintf(sndbuff, sizeof(sndbuff), "%s", CMD_CREATE);
	send(cli_fd, sndbuff, strlen(sndbuff), 0);
}

static void cmd_reg(void)
{
	char sndbuff[2018];
	char buf[64];

	if (err_check()) return;

	cli_without_hint("%s", "please input your name:");
	scanf("%s", buf);	
	snprintf(sndbuff, sizeof(sndbuff), "%s:%s", CMD_REG, buf);
	send(cli_fd, sndbuff, strlen(sndbuff), 0);
}

static void cmd_list(void)
{
	char sndbuff[2018];
	if (err_check()) return;
	
	snprintf(sndbuff, sizeof(sndbuff), "%s", CMD_LIST);
	send(cli_fd, sndbuff, strlen(sndbuff), 0);
}

static void cmd_join(void)
{
	char sndbuff[2018];
	char buf[128];

	if (err_check()) return;

	cli_without_hint("%s", "please input game name:");
	scanf("%s", buf);	

	snprintf(sndbuff, sizeof(sndbuff), "%s:%s", CMD_JOIN, buf);
	send(cli_fd, sndbuff, strlen(sndbuff), 0);
}

static void cmd_exit(void)
{
	char sndbuff[2018];

	if (err_check()) return;

	snprintf(sndbuff, sizeof(sndbuff), "%s", CMD_EXIT);
	send(cli_fd, sndbuff, strlen(sndbuff), 0);
}

static void cmd_dereg(void)
{
	char sndbuff[2018];

	if (err_check()) return;

	snprintf(sndbuff, sizeof(sndbuff), "%s", CMD_DREG);
	send(cli_fd, sndbuff, strlen(sndbuff), 0);
}

static void cli_cmd(void)
{
	char buf[128];
	cmd_help();
	while (1) {
		scanf("%s", buf);
		//printf("you input :%s, len: %u\n", buf, strlen(buf));
		if (!strncmp(buf, CMD_CREATE, strlen(buf))) {
			/* you create game */
			cmd_create();
		} else if (!strncmp(buf, CMD_REG, strlen(buf))) {
			/* you reg game */
			cmd_reg();
		} else if (!strncmp(buf, CMD_LIST, strlen(buf))) {
			/* you list game */
			cmd_list();
		} else if (!strncmp(buf, CMD_JOIN, strlen(buf))) {
			/* you join game */
			cmd_join();
		} else if (!strncmp(buf, CMD_EXIT, strlen(buf))) {
			/* you exit game */
			cmd_exit();
		} else if (!strncmp(buf, CMD_DREG, strlen(buf))) {
			/* you dereg gamei */
			cmd_dereg();
		} else if (!strncmp(buf, "help", strlen("help"))) {
			/* you dereg gamei */
			cmd_help();
		} else {
			/* message send to svr */
			char sendbuf[1024];
			snprintf(sendbuf, sizeof(sendbuf), "%s:%s", CMD_MSG, buf);
			send(cli_fd, sendbuf, strlen(sendbuf), 0);
		}
	}

}


static void cli_start(char *svr_ip, char *svr_port)
{
    int sockfd = 0, n = 0;
    char recvBuff[1024];
    pthread_t tid;
    struct sockaddr_in serv_addr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    memset(&serv_addr, 0, sizeof(struct sockaddr_in));

    serv_addr.sin_family = AF_INET;
    //serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    inet_aton(svr_ip, &serv_addr.sin_addr);
    //serv_addr.sin_addr.s_addr = inet_aton(svr_ip, &serv_addr.sin_addr.s_addr);
    serv_addr.sin_port = htons(atoi(svr_port));

    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\n Error : Connect Failed \n");
        return;
    }
    cli_fd = sockfd;
    pthread_create(&tid, NULL, cli_handle, NULL);
    //cli_handle(sockfd);
    cli_cmd();
}

void *cli(void *arg)
{
    char svr_ip[32];
    char svr_port[8];

    printf("cli started.\n#svr>\n");
    cli_hint("Please input svr ip, example 127.0.0.1\n");
    scanf("%s", svr_ip);
    cli_hint("Please input port, example 3333\n");
    scanf("%s", svr_port);
    cli_hint("you input svr ip: %s:%s\n", svr_ip, svr_port);

    cli_start(svr_ip, svr_port);
}


