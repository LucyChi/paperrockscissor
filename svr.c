#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <fcntl.h>

#include "comm.h"
#include "list.h"

#define svr_hint(fmt, args...) fprintf(stderr, fmt "#svr>" , \
             ##args)

struct list_head cli_list;

enum play_state {
	P_NOT_JOIN = 0,
	P_JOIN = 1,
};

struct game;

struct cli {
	struct list_head l;

	int fd;
	int state;
	int value;
	char name[32];
	int player_idx;
        struct game *g;
};

enum game_state {
	NOT_CREATE	= 0,
	CREATED 	= 1,
	READY 		= 2,
};

struct game {
        char name[128];
        struct list_head l;
	int state;
	int player_num;
	struct cli *c[2];
};

static struct game g;
struct list_head game_list;

static void cli_init(struct cli *c)
{
        c->fd = 0;
        c->name[0] = 0;
        c->value = 0;
        c->state = 0;
        c->player_idx = -1;
}

static void svr_cmd_create(struct cli *c)
{
    char sendbuff[1024];
    if (c->state == P_JOIN) {
        snprintf(sendbuff, sizeof(sendbuff), "%s",
                "You are in game, can not create again!");
        send(c->fd, sendbuff, strlen(sendbuff), 0);
    } else if (c->name[0] == 0) {
        snprintf(sendbuff, sizeof(sendbuff), "%s",
                "Please reg first!");
        send(c->fd, sendbuff, strlen(sendbuff), 0);
    } else {
        c->state = P_JOIN;
        /**/
        struct game *g = calloc(1, sizeof(*g));
        snprintf(g->name, sizeof(g->name), "%s-%s",
                "rps", c->name);
        g->player_num++;
        g->state = CREATED;
        g->c[0] = c;
        
        c->player_idx = 0;
        c->g = g;

        list_add_tail(&g->l, &game_list);
        /* send out hint */
        snprintf(sendbuff, sizeof(sendbuff), "%s",
                "please choose: 1, rock, 2, paper, 3, scissor");
        send(c->fd, sendbuff, strlen(sendbuff), 0);
    }
}

static void svr_cmd_reg(struct cli *c, char *recvbuff)
{
    char sendbuff[1024];
    char *ptr = strchr(recvbuff, ':');
    printf("cli reg: %s\n", recvbuff);

    if (ptr == NULL) {
        snprintf(sendbuff, sizeof(sendbuff), "%s", "please reg with your name");
        send(c->fd, sendbuff, strlen(sendbuff), 0);
        //send(c->fd, sendbuff)
    } else {
        ptr++;
        snprintf(c->name, sizeof(c->name), "%s", ptr);
        snprintf(sendbuff, sizeof(sendbuff), "welcome %s, %s", c->name,
                "you are ready to create the game if others haven't created already.");
        send(c->fd, sendbuff, strlen(sendbuff), 0);
    }
}

static void svr_cmd_join(struct cli *c, char *cmd)
{
    char sendbuff[1024];
    if (c->name[0] == 0) {
        snprintf(sendbuff, sizeof(sendbuff), "Please reg first");
        send(c->fd, sendbuff, strlen(sendbuff), 0);
        return;
    }
    if (c->state == P_JOIN) {
        snprintf(sendbuff, sizeof(sendbuff), "You have joined game: %s, you need exit",
                c->g->name);
        send(c->fd, sendbuff, strlen(sendbuff), 0);
        return;
    } else {
        /* find game name */
        struct game *g, *next;
        int find_flag = 0;

        char *ptr = strchr(cmd, ':');
        if (!ptr) {
            snprintf(sendbuff, sizeof(sendbuff), "%s", "Please input game name");
            send(c->fd, sendbuff, strlen(sendbuff), 0);
            return;
        }
        ptr++;
        list_for_each_entry_safe(g, next, &game_list, l) {
            printf("compare game: %s and ask:%s\n", g->name, ptr);
            if (!strcmp(g->name, (ptr))) {
                find_flag = 1;
                break;
            }
        }
        if (!find_flag) {
            snprintf(sendbuff, sizeof(sendbuff), "Can not find game: %s",
                    (ptr));
            send(c->fd, sendbuff, strlen(sendbuff), 0);
        } else {
            if (g->player_num == 2) {
                snprintf(sendbuff, sizeof(sendbuff),
                        "game is ongoing, please join other games");
                send(c->fd, sendbuff, strlen(sendbuff), 0);
                return;
            }
            if (g->state != P_JOIN) {
                printf("Oops, game state wrong\n");
                exit(0);
            }
            g->player_num++;
            g->c[1] = c;

            c->g = g;
            snprintf(sendbuff, sizeof(sendbuff), "%s",
                    "please choose: 1, rock, 2, paper, 3, scissor");
            send(c->fd, sendbuff, strlen(sendbuff), 0);
        }
    }
}

static void svr_cmd_exit(struct cli *c)
{
    char sendbuff[1024];
    if (c->state != P_JOIN) {
            snprintf(sendbuff, sizeof(sendbuff), "%s",
                    "You are not in game");
            send(c->fd, sendbuff, strlen(sendbuff), 0);
            return;
    }
    if (c->player_idx == 0) {
        /* game owner */
        if (c->g->player_num == 2) {
            /* other people in game */
            snprintf(sendbuff, sizeof(sendbuff), "%s",
                    "game owner exit");
            send(c->g->c[1]->fd, sendbuff, strlen(sendbuff), 0);
            c->g->c[1]->state = P_NOT_JOIN;
            c->g->c[1]->player_idx = -1;
            c->g->c[1]->value = 0;
        }
        /* delete game */
        list_del(&c->g->l);
        free(c->g);
    }
    snprintf(sendbuff, sizeof(sendbuff), "%s",
            "your exit game");
    send(c->fd, sendbuff, strlen(sendbuff), 0);
    c->state = P_NOT_JOIN;
    c->player_idx = -1;
    c->value = 0;

}

static void svr_cmd_dereg(struct cli *c)
{
    char sendbuff[1024];
    if (c->state == P_JOIN) {
        snprintf(sendbuff, sizeof(sendbuff), "%s",
                "your need exit game first");
        send(c->fd, sendbuff, strlen(sendbuff), 0);
        return;
    }
    snprintf(sendbuff, sizeof(sendbuff), "%s",
            "dereg successfully");
    send(c->fd, sendbuff, strlen(sendbuff), 0);
    c->name[0] = 0;
}

static void svr_cmd_list(struct cli *c)
{
    char sendbuff[1024];
    int n = 0;
    struct game *g, *next;

    list_for_each_entry_safe(g, next, &game_list, l) {
        snprintf(sendbuff, sizeof(sendbuff), "game #%d:%s\n",
                n++, g->name);
        send(c->fd, sendbuff, strlen(sendbuff), 0);
    }
    if (n == 0) {
        snprintf(sendbuff, sizeof(sendbuff), "%s",
                "No game avaliabel, you can reate one. :)");
        send(c->fd, sendbuff, strlen(sendbuff), 0);
    }
}

static void reset_game(struct game *g)
{
    g->c[0]->value = 0;
    g->c[1]->value = 0;
}

static void svr_cmd_msg(struct cli *c, char *recvbuff)
{
    char sendbuff[1024];
    char *ptr = strchr(recvbuff, ':');
    ptr++;

    if (strlen(ptr) > 1) {
        snprintf(sendbuff, sizeof(sendbuff), "%s",
                "unkown cmd");
        send(c->fd, sendbuff, strlen(sendbuff), 0);
        return; 
    }

    c->value = atoi(ptr);
    printf("c:%p, choose value:%d, in game: %p, player_num: %d\n",
            c, c->value, c->g, c->g->player_num);
    /**/
    if (c->g->player_num == 2) {
        int other = 0;
        if (c->player_idx == 0) other = 1;
        if (c->g->c[0]->value <= 0 || c->g->c[1]->value <= 0) {
            return;
        }

        if (c->g->c[0]->value == c->g->c[1]->value) {
            snprintf(sendbuff, sizeof(sendbuff), "%s",
                    "you are tie!!");
            send(c->g->c[0]->fd, sendbuff, strlen(sendbuff), 0);
            send(c->fd, sendbuff, strlen(sendbuff), 0);
            reset_game(c->g);
            return;
        }

        if (c->value == 1) {
            if (c->g->c[other]->value == 2) {
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        "you are winner!!");
                send(c->g->c[other]->fd, sendbuff, strlen(sendbuff), 0);
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        ":( try next around !!");
                send(c->fd, sendbuff, strlen(sendbuff), 0);
                reset_game(c->g);
            }
            if (c->g->c[other]->value == 3) {
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        ":( try next around !!");
                send(c->g->c[other]->fd, sendbuff, strlen(sendbuff), 0);
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        "you are winner!!");
                send(c->fd, sendbuff, strlen(sendbuff), 0);
                reset_game(c->g);
            }
        }
        if (c->value == 2) {
            if (c->g->c[other]->value == 3) {
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        "you are winner!!");
                send(c->g->c[other]->fd, sendbuff, strlen(sendbuff), 0);
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        ":( try next around !!");
                send(c->fd, sendbuff, strlen(sendbuff), 0);
                reset_game(c->g);
            }
            if (c->g->c[other]->value == 1) {
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        ":( try next around !!");
                send(c->g->c[other]->fd, sendbuff, strlen(sendbuff), 0);
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        "you are winner!!");
                send(c->fd, sendbuff, strlen(sendbuff), 0);
                reset_game(c->g);
            }
        }
        if (c->value == 3) {
            if (c->g->c[other]->value == 1) {
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        "you are winner!!");
                send(c->g->c[other]->fd, sendbuff, strlen(sendbuff), 0);
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        ":( try next around !!");
                send(c->fd, sendbuff, strlen(sendbuff), 0);
                reset_game(c->g);
            }
            if (c->g->c[other]->value == 2) {
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        ":( try next around !!");
                send(c->g->c[other]->fd, sendbuff, strlen(sendbuff), 0);
                snprintf(sendbuff, sizeof(sendbuff), "%s",
                        "you are winner!!");
                send(c->fd, sendbuff, strlen(sendbuff), 0);
                reset_game(c->g);
            }
        }
    }
}

static void cli_handle(struct cli *c)
{
	char sendbuff[1024], recvbuff[1024];
	int n;

	if (!c) return;	
	n = recv(c->fd, recvbuff, sizeof(recvbuff), 0);
	if (n < 0){
		return;
	}
	if (n == 0) {
		printf("socket closed by peer side: %d\n",c->fd);
		list_del(&c->l);
                /* if already joined game */
                if (c->g) {
                    c->g->player_num--;
                    c->g->c[c->player_idx] = NULL;
                }
		close(c->fd);
		free(c);
		return;
	}
	recvbuff[n] = 0;	
	printf("get from fd:%d, %s\n", c->fd, recvbuff);
        if (!strncmp(recvbuff, CMD_CREATE, strlen(CMD_CREATE))) {
            svr_cmd_create(c);
        }

	if (!strncmp(recvbuff, CMD_REG, strlen(CMD_REG))) {
                svr_cmd_reg(c, recvbuff);
        }

	if (!strncmp(recvbuff, CMD_LIST, strlen(CMD_LIST))) {
            svr_cmd_list(c);
        }

	if (!strncmp(recvbuff, CMD_JOIN, strlen(CMD_JOIN))) {
            svr_cmd_join(c, recvbuff);
        }
        /* exit game */
	if (!strncmp(recvbuff, CMD_EXIT, strlen(CMD_EXIT))) {
            svr_cmd_exit(c);
        }

	if (!strncmp(recvbuff, CMD_DREG, strlen(CMD_DREG))) {
            svr_cmd_dereg(c);
	}

	if (!strncmp(recvbuff, CMD_MSG, strlen(CMD_MSG))) {
            svr_cmd_msg(c, recvbuff);
	}
}

static void svr_start(char *svr_ip, char *svr_port)
{
    int listenfd = 0, connfd = 0;
    struct sockaddr_in serv_addr;

    listenfd = socket(AF_INET, SOCK_STREAM, 0);

    fcntl(listenfd, F_SETFL, O_NONBLOCK);

    memset(&serv_addr, 0, sizeof(struct sockaddr_in));

    serv_addr.sin_family = AF_INET;
    //serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    inet_aton(svr_ip, &serv_addr.sin_addr);
    //serv_addr.sin_addr.s_addr = inet_aton(svr_ip, &serv_addr.sin_addr.s_addr);
    serv_addr.sin_port = htons(atoi(svr_port));

    bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr));

    listen(listenfd, 10);

    svr_hint("serve start listening on %s\n", svr_port);

    while(1)
    {
            {
                connfd = accept(listenfd, (struct sockaddr*)NULL, NULL);
                if (connfd > 0) {
		
		   printf("client connected to svr: %d.\n", connfd);
		   struct cli *c = calloc(1, sizeof(struct cli));
		   if (!c) {
			   printf("out of mem\n");
			   close(connfd);
		   }
		   c->fd = connfd;
                   c->player_idx = -1;
		   fcntl(c->fd, F_SETFL, O_NONBLOCK);
            	   //send(c->fd, send_str, strlen(send_str), 0);
		   list_add_tail(&c->l, &cli_list);
                }
            } 
	    {
		    //printf("before list******\n");
		    if (!list_empty(&cli_list)) {
			    struct cli *c, *next;
                            list_for_each_entry_safe(c, next, &cli_list, l)   {
				    cli_handle(c);
				    if (list_empty(&cli_list)) break;
			    }
		    }


	    }
        usleep(1000);
    }
}

void *svr(void *arg)
{
    char svr_ip[32];
    char svr_port[8];

    printf("svr started.\n#svr>\n");
    svr_hint("Please input svr ip, example 127.0.0.1\n");
    scanf("%s", svr_ip);
    svr_hint("Please input port, example 3333\n");
    scanf("%s", svr_port);
    svr_hint("you input svr ip: %s:%s\n", svr_ip, svr_port);
    list_init(&cli_list);
    list_init(&game_list);
    svr_start(svr_ip, svr_port);
    list_init(&cli_list);
    list_init(&game_list);
}


