#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <pthread.h>

pthread_t tid;

extern void *svr(void *arg);
extern void *cli(void *arg);

int main(int argc, char **argv)
{
    int flag = 1;
    if (argc < 2) {
        printf("Usage: %s [1|0]\n", argv[0]);
        return 0;
    }
    flag = atoi(argv[1]);
    if (flag == 0) {
        printf("run as client\n");
        pthread_create(&tid, NULL, &cli, NULL);
    } else {
        printf("run as server");
        pthread_create(&tid, NULL, &svr, NULL);
    }
    for (;;) {
        sleep(1);
    }

}
