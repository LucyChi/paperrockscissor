cmake_minimum_required(VERSION 2.8.9)
project (hello)

LINK_DIRECTORIES(/usr/lib/x86_64-linux-gnu/)
set (CMAKE_C_FLAGS "-O0 -g")
link_libraries(
    "pthread"
    )

set(SRC
    main.c
    svr.c
    cli.c
    )


add_executable(game
    ${SRC})

#TARGET_LINK_LIBRARIES(hello uv)
